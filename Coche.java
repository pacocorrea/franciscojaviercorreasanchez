class Coche {

	//------------------------------------------------------------------variables

	private String matricula;
	private String marca;
	private String modelo;
	private String color;
	private boolean techoSolar;
	private float kilometros;
	private int numPuertas;
	private int numPlazas;
	private static int numCoches; //dispone de un get
	public static final MAX_COCHES = 5; //no tiene ni get ni set




	//--------------------------------------------------------------setters y getters


	public void setMatricula(String valorMatricula) {
		matricula = valorMatricula;
	}

	public String getMatricula() {
		return matricula;
	}


	public void setMarca(String valorMarca) {
		marca = valorMarca;
	}

	public String getMarca() {
		return marca;
	}


	public void setModelo(String valorModelo) {
		modelo = valorModelo;
	}

	public String getModelo() {
		return modelo;
	}


	public void setColor(String valorColor) {
		color = valorColor;
	}

	public String getColor() {
		return color;
	}


	public void setTechoSolar(boolean valorTechoSolar) {
		techoSolar = valorTechoSolar;
	}

	public boolean getTechoSolar() {
		return techoSolar;
	}


	public void setKilometros(float valorKilometros) {
		if (valorKilometros >= 0) {
				kilometros = valorKilometros;
		}
	}

	public float getKilometros() {
		return kilometros;
	}


	public void setNumPuertas(int valorNumPuertas) {
		if (valorNumPuertas > 0) && (valorNumPuertas <=5) {
			numPuertas = valorNumPuertas;
		}
	}

	public int getNumPuertas() {
		return numPuertas;
	}


	public void setNumPlazas(int valorNumPlazas) {
		if ((valorNumPlazas > 0) && (valorNumPlazas <=7)) {
			numPlazas = valorNumPlazas;
		} 
	}

	public int getNumPlazas() {
		return numPlazas;
	}

	public static int getNumCoches() {
		return numCoches;
	}
	

	//---------------------------------------------------------constructores

	public Coche() {
		matricula = "";
		marca = "SEAT";
		modelo = "ALTEA";
		color = "blanco";
		techoSolar = false;
		kilometros = 0.0F;
		numPuertas = 3;
		numPlazas = 5;
		numCoches ++;
		//usar la variable numcoches para incrementar cada vez que se fabrique un nuevo coche
	}

	public Coche(String matri) {
		this();
		setMatricula(matri);
	}

	public Coche(int numPuer, int numPla) {
		this();
		setNumPuertas(numPuer);
		setNumPlazas(numPla);
	}

	public Coche(String mar, String mod, String col) {
		this();
		setMarca(mar);
		setModelo(mod);
		setColor(col);
	}

	



	//-------------------------------------------------------------métodos 

	public void avanzar(float avanzarkms) {
		kilometros = kilometros + avanzarkms;
		
	}

	public void tunear() {
		kilometros = 0.0F;
		
		if (techoSolar = false) {
			techoSolar = true;
		} 	
	}

	public void tunear(String nuevoColor) {
		this.tunear();
		color = nuevoColor;
		
	}

	public void matricular(String asignaMatricula) {
		matricula = asignaMatricula;
		
	}
}